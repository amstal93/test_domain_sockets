//
//  main.c
//  counter
//
//  Created by Danny Goossen on 19/5/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/un.h>
#include <strings.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <signal.h>
#include <stdlib.h>
#include <getopt.h>
#include <sys/time.h>

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

#ifndef GITVERSION
#define GITVERSION "no_git"
#endif

/* Socket name of the counter*/
#ifndef SRV_SOCK
#define SRV_SOCK "count.sock"
#endif

/* Directory to put client socket in */
#ifndef SOCK_DIR
#define SOCK_DIR "sockets"
#endif

/* default verbose */
#ifndef VERBOSE_YN
#define VERBOSE_YN 1
#endif

/* default verbose */
#ifndef START_DELAY
#define START_DELAY 0
#endif

char socket_name[256];
unsigned long long count;

struct parameter_t
{
   char socket_dir[256];
   char srv_socket[256];
   u_int8_t verbose;
   u_int8_t quiet;
   u_int8_t debug;
   u_int8_t start_delay;
	u_int64_t interval;
};

static struct parameter_t parameters;

void usage( int exitcode)
{
   fprintf(stderr, "Usage: counter [--options] \n");
   fprintf(stderr, "          [options]     \n\n");
   fprintf(stderr, "version          : Shows the version and usage\n");
   fprintf(stderr, "help             : Shows the version and usage\n");
   fprintf(stderr, "verbose          : turns on verbose output mode\n");
   fprintf(stderr, "debug            : turns on debug output mode\n");
   fprintf(stderr, "quiet            : quiet down output\n");
   fprintf(stderr, "interval=10000   : output requests/sec every 10K requests\n");
   fprintf(stderr, "sock_dir=sockets     : specifies the socket directory as prefex\n");
   fprintf(stderr, "cli_sock=client.sock : specifies the socket name we listen on\n");
   fprintf(stderr, "start_delay=10    : delayed start in milliseconds\n");
   fprintf(stderr, "\n");
   fprintf(stderr, "Defaults: verbose    = %d\n",   VERBOSE_YN);
   fprintf(stderr, "          sock_name  = %s\n",   SRV_SOCK);
   fprintf(stderr, "          sock_dir   = %s\n",   SOCK_DIR);
   
   fprintf(stderr, "\n");
   
   exit(exitcode);
}


/*------------------------------------------------------------------------
 * int process_options(int argc, char **argv, tb_parameter_t *parameter)
 *
 * Process the program argument options and set the parameters accordingly
 *------------------------------------------------------------------------*/
int process_options(int argc, char **argv, struct parameter_t *parameter)
{
   
   struct option long_options[] = {
      { "verbose",    0, NULL, 'v' },
      { "quiet",      0, NULL, 'q' },
      { "sock_name",   1, NULL, 'n' },
      { "sock_dir",   1, NULL, 'D' },
      { "help",       0, NULL, 'j' },
      { "version",    0, NULL, 'g' },
      { "start_delay",    1, NULL, 'r' },
      { "debug",      0, NULL, 'd' },
	   { "interval",      1, NULL, 'i' },
      { NULL,         0, NULL, 0 } };
   
   int           which;
   optind=1;
   if (argc>1)
   {
      /* for each option found */
      while ((which = getopt_long(argc, argv, "+", long_options, NULL)) > 0) {
         
         /* depending on which option we got */
         switch (which) {
               /* --verbose    : enter verbose mode for debugging */
            case 'v':
            {
               if (parameter->quiet)
               {
                  fprintf(stderr,"Invalid option, choose quiet or verbose\n");
                  return -1;
               }
               else
               {
                  parameter->verbose = 1;
               }
            }
               break;
            case 'q':
            {
               if (parameter->verbose)
               {
                  fprintf(stderr,"Invalid option, choose quiet or verbose\n");
                  return -1;
               }
               else
                  parameter->quiet = 1;
            }
               break;
            case 'n': {
               sprintf(parameter->srv_socket,"%s",optarg);
            }
               break;
            case 'D': {
               sprintf(parameter->socket_dir,"%s",optarg);
            }
               break;
            case 'r': {
               parameter->start_delay   = atoi(optarg);
            }
               break;
			 case 'i': {
				 parameter->interval   = atoi(optarg);
			 }
				 break;
            case 'g':
            {
               
               printf("%s\n",GITVERSION);
               exit(0);
            }
            case 'j': {
               printf("\ncounter Git Version: %s\n \n",GITVERSION);
               usage(0);
               break;
            }
            case 'd': {
               parameter->debug = 1;
            }
               break;
            
               /* otherwise    : display usage information */
            default:
               ;
               usage(1);
         }
      }
   }
   // if NULL then no option was given and we default define the dir where tblastd was started !!!!!!
   //value = getenv(name);
   return optind;
}

/*------------------------------------------------------------------------
 * void rset_missing_parms(tb_parameter_t *parameter)
 *
 * set the parameters to default if not in commandline or config
 *------------------------------------------------------------------------*/
void set_missing_parms(struct parameter_t *parameter)
{
   if (strlen(parameter->srv_socket) == 0)
   {
      sprintf(parameter->srv_socket,"%s",SRV_SOCK);
   }
   
   if (strlen(parameter->socket_dir) == 0)
   {
      sprintf(parameter->socket_dir,"%s",SOCK_DIR);
   }
   if (! parameter->verbose && !parameter->quiet) parameter->verbose = VERBOSE_YN;

   sprintf(socket_name,"%s/%s",parameter->socket_dir,parameter->srv_socket);
   return;
}


static void signal_handler(sig)
int sig;
{
   int i;
   switch(sig) {
      case SIGHUP:
         count=0;
         break;
      case SIGSTOP:
      case SIGQUIT:
      case SIGTERM:
      case SIGKILL:
      case SIGINT:
        fprintf (stderr,"\nEXIT: Signal %d : %s !!!\n",sig,strsignal(sig));
         for (i=getdtablesize();i>=2;--i)
         {
            close(i); /* close all non std descriptors */
         }
        unlink(socket_name);
        exit(0);
        break;
   }
}

void init_signals()
{
   signal(SIGHUP,&signal_handler); /* catch hub signal */
   signal(SIGTERM,&signal_handler); /* catch term signal */
   signal(SIGSTOP,&signal_handler); /* catch stop signal */
   signal(SIGINT, &signal_handler); /* catch int signal */
   signal(SIGQUIT, &signal_handler); /* catch quit signal */
   signal( SIGKILL, &signal_handler); /* catch kill signal */
}

int bind_unix_socket(char * sock_name)
{
   int sockid=INVALID_SOCKET;
   struct sockaddr_un serv_addr;
   bzero((char *) &serv_addr, sizeof(serv_addr));
   serv_addr.sun_family = AF_UNIX;
   if (!sock_name && strlen(sock_name)==0) return sockid;
   strcpy(serv_addr.sun_path, sock_name);
   int servlen=(int)strlen(serv_addr.sun_path) + 1 + (int)sizeof(serv_addr.sun_family);
   sockid=socket(AF_UNIX,SOCK_DGRAM,0);
   if(bind(sockid,(struct sockaddr*)&serv_addr,servlen)==SOCKET_ERROR)
   {
      perror("Bind error");
      close(sockid);
      return (INVALID_SOCKET);
   }
   chmod(sock_name, 0777);
   return sockid;
}


int main(int argc, char ** argv)
{
   bzero(&parameters, sizeof(parameters));
   init_signals();
   count=0;
   size_t count_len=sizeof(count);
   int this_error=0;
   int flags=0;
   size_t write_size=0;
   size_t read_size=0;
   char request='\0';
   struct sockaddr_un client_addr;
   bzero((char *) &client_addr, sizeof(client_addr));
   socklen_t client_addr_size=sizeof(client_addr);
   struct timeval tv_now,tv_start;
	u_int32_t interpacket_time=0;
	u_int64_t last_count=0;
   if (process_options(argc, argv,&parameters) < 0)
   {
      fprintf(stderr, " *** ERROR in command line options, exit \n");
      return (-1);
   }

   set_missing_parms(&parameters);
   if (parameters.start_delay) usleep(100000*parameters.start_delay);
   unlink(socket_name);
   int sockid=bind_unix_socket(socket_name);
   if (sockid==INVALID_SOCKET) return 1;
   fprintf(stderr,"debug: counter rdy for action\n");

   for(;;) // the main write loop! blocking socket
   {
	  bzero((char *) &client_addr, sizeof(client_addr));
      do
      {
         read_size = recvfrom(sockid,&request, 1, flags ,(struct sockaddr*)&client_addr, &client_addr_size );
      } while(read_size ==SOCKET_ERROR && (this_error=errno)!=EINTR);

      if ( read_size==SOCKET_ERROR && this_error!=EINTR)
      {
         fprintf(stderr,"Read error: %s\n",strerror(this_error));
         close(sockid);
         unlink(socket_name);
      }
      else if (read_size == 1)
      {
		  if (parameters.interval && count==0)
		  {
			  gettimeofday(&tv_start, NULL);
		  }
		  switch (request){
             case '=': break;
             case '+': count++; break;
             case '-': count--; last_count=0; break;
			  case '0': count=0; last_count=0;break;
             default: count++;
         }
		  if (parameters.interval && count !=0 && request!='=' && request!='0' && request!='-' && (count % parameters.interval) == 0)
		  {
			  gettimeofday(&tv_now, NULL);
			  if ((tv_now.tv_usec- tv_start.tv_usec)>=0)
			  {
				  interpacket_time= (tv_now.tv_usec- tv_start.tv_usec);
				  if ((tv_now.tv_sec - tv_start.tv_sec)>0)
					  interpacket_time += (tv_now.tv_sec - tv_start.tv_sec)*1000000;
			  }
			  else
			  {
				  interpacket_time= (tv_now.tv_usec + 1000000 - tv_start.tv_usec);
				  if ((tv_now.tv_sec-1 - tv_start.tv_sec)>0)
					  interpacket_time += (tv_now.tv_sec- 1 - tv_start.tv_sec)*1000000;
			  }
			  if (interpacket_time > 0 )
			  {
				  u_int32_t speed=(u_int32_t) ((count -last_count)* 1000000  / (interpacket_time));
				  fprintf(stderr,"%u requests/sec\n",speed );
				  last_count=count;
				  tv_start.tv_sec=tv_now.tv_sec;
				  tv_start.tv_usec=tv_now.tv_usec;
			  }
		  }
 
         do
         {
            write_size = sendto(sockid,&count, count_len, MSG_DONTWAIT,(struct sockaddr*)&client_addr, client_addr_size );
         } while(write_size ==SOCKET_ERROR && (this_error=errno)!=EINTR);
         if ( write_size==SOCKET_ERROR)
         {
            fprintf(stderr,"Write error: %s\n",strerror(this_error));
		 }
      }
   }
   return 0;
}
