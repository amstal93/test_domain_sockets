# counter

[![build status](https://gitlab.com/dgoo2308/test_domain_sockets/badges/master/build.svg)](https://gitlab.com/dgoo2308/test_domain_sockets/commits/master)

counter is program that listens on a unix-socket, with a datagram protocol, and for each request as:
1. '+' : it will increase the count and return the count as an u_int64_t
2. '0' : resets the count and return the 0 value as an u_int64_t
3. '=' : returns the count as an u_int64_t, without
4. '-' : decreases count with one and returns count as an u_int64_t

## purpose

have multiple processes on a computer to share a `counter`.

## specific purpose

share a counter for docker to docker communication.

## why

for high volume request, reaction time and process time outperforms any database/networksockets.

## concept

Here was chosen to use named-sockets with datagrams, allowing for the fasted possible Inter Container Communication without specific IPC through memory sharing techniques.

## tests:

the build also includes a test, having with docker-compose to scale 10 containers who all sent requests to increase the counter to the count-container.

## conclusing:

Very fast counter
